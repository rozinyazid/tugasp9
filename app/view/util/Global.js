Ext.define("Covid.util.Globals",{
    singleton : true,
    alternateClassName: 'globalUtils',
    version:'1.0',
    config: {
	    phppath: 'https://gitlab.com/rozinyazid/covidarea/resources'
    },
    constructor : function(config) {
        this.initConfig(config);
    },

    startRecordCordova: function(){
	    let opts = {limit:1}
	    navigator.device.capture.captureAudio(globalUtils.captureSuccess, globalUtils.captureError, opts);
	},
	captureSuccess: function(mediaFiles){
	    var i, path, len;
	    name = mediaFiles[0].name;
	    fileURL = mediaFiles[0].fullPath;
        type = mediaFiles[0].type;
        size = mediaFiles[0].size;
        if(size>80000){
        	document.getElementById('recordingInfo').textContent = "Rekaman Anda terlalu panjang, silakan coba lagi.";        
        }
        else{
        	document.getElementById('recordingInfo').textContent = "Menyimpan rekaman suara...";     
        }					  
	},
	captureError: function(error){
	    document.getElementById('recordingInfo').textContent = 'Error code: ' + error.code;
	},

	// Fungsi callback untuk proses upload
	uploadSuccess: function(r){
		if(r.response=="failed"){
	    	document.getElementById('recordingInfo').textContent = "Rekaman gagal disimpan.";	    	
	    }	    
	    else{	    	
	    	document.getElementById('recordingInfo').textContent = "Rekaman berhasil disimpan.";
	    }
	},
	uploadError: function(error){
		Ext.Msg.alert("An error has occurred: Code = " + error.code);	    
	}
});