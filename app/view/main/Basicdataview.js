Ext.define('tugasp9.view.dataview.Basicdataview', {
    extend: 'Ext.Container',
    xtype: 'detail',
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'tugasp9.store.Detail'
    ],

    
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    
    items: [{
        xtype: 'dataview',
        scrollable: 'y',
        cls: 'dataview-basic',
        itemTpl: 'Name : {name}<br>Email: {email}<br>Phone: {phone} <hr> <br> id: {id}',
        store: 'detail',
        plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
    
                    '<tr><td>Name: </td><td>{name}</td></tr>' +
                    '<tr><td>Email:</td><td>{email}</td></tr>' + 
                    '<tr><td>Phone:</td><td>{phone}</td></tr>'
        }
    }]
});