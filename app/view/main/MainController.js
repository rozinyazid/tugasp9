/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('tugasp9.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onItemSelected: function (sender, record) {
        Ext.getStore('personnel').filter('name', record.data.name)
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },

    onReadClicked: function(){
       Ext.getStore('personnel').load();
    }
});
