Ext.define('tugasp9.store.Personnel', {
    extend: 'Ext.data.Store',

    alias: 'store.personnel',
    storeId: 'personnel',
    //autoLoad: true,     
   
    fields: [
     'name', 'email', 'phone'
    ],

    proxy: {
        type: 'jsonp',
        api: {          read: "http://localhost/MyApp_php/readPersonnel.php" ,
                        update: "http://localhost/MyApp_php/updatePersonnel.php",
                        destroy: "http://localhost/MyApp_php/destroyPersonnel.php"

    }, 
        reader: {
            type: 'json',
            rootProperty: 'items',
            messageProperty : 'error'
        }
    },
    listeners: { 
        beforeload: function(store,operation, e0pts){
            this.getProxy().setExtraParams({
                user_id: 7
            });
        }
    }       
});
