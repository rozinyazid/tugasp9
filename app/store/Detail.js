Ext.define('tugasp9.store.Detail', {
    extend: 'Ext.data.Store',

    alias: 'store.detail',
    storeId: 'detail',
    //autoLoad: true,
 
    fields: [
         'name', 'email', 'phone'
    ],


    proxy: {
        type: 'jsonp',
        api: {
            read: "http://localhost/MyApp_php/readPersonnel.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
